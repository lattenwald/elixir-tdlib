TDLIBJSONCLI_SRC=tdlib-json-cli

BUILD_ROOT=_build
BUILD_DIRECTORY=$(BUILD_ROOT)/$(TDLIBJSONCLI_SRC)/build
BUILD_ARTIFACT=$(BUILD_DIRECTORY)/bin/tdlib_json_cli
PRIV_DIR=priv
BIN_TARGET=$(PRIV_DIR)/tdlib-json-cli

JOBS=$(nproc)
VERBOSE=0

all:
	if [ ! -f $(BIN_TARGET) ]; then \
		make extract build import; \
	fi

clean:
	rm -f $(BIN_TARGET)
	rm -r $(BUILD_ROOT)

extract:
	mkdir -p $(BUILD_ROOT) &&  \
	cd $(BUILD_ROOT) && \
	ln -sf ../$(TDLIBJSONCLI_SRC)

build:
	mkdir -p $(BUILD_DIRECTORY) && \
	cd $(BUILD_DIRECTORY) && \
	cmake -DCMAKE_BUILD_TYPE=Release .. && \
	make -j$(JOBS) VERBOSE=$(VERBOSE)

import:
	cp $(BUILD_ARTIFACT) $(BIN_TARGET)
	[ -f $(BUILD_ROOT)/types.json ] && cp $(BUILD_ROOT)/types.json $(PRIV_DIR)/types.json || echo "No $(BUILD_ROOT)/types.json, ohwell"
